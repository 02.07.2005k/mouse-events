"use strict"
// Теоретичні питання
// 1) Події в JavaScript - це дії, які відбуваються у веб-додатку, такі як кліки, 
// натискання клавіш, переміщення миші, завантаження сторінки тощо.
// Вони використовуються для реагування на дії користувачів або інші події, які виникають у веб-додатку.

// 2) У JavaScript доступні ткакі події миші, такі як click, mousedown, mouseup, mousemove, mouseover, mouseout тощо.
// Наприклад, click відбувається, коли користувач клікає на елементі, mousedown відбувається, 
// коли кнопку миші натиснуто над елементом, mouseup - коли кнопку миші відпускають після натискання, 
// mousemove - коли мишу рухають над елементом, mouseover - коли миша наводиться на елемент і так далі.

// 3) Подія "contextmenu" відбувається, коли користувач натискає праву кнопку миші на елементі.
// Вона використовується для створення контекстного меню, яке з'являється при натисканні правою кнопкою миші. 
// Це може бути корисно для реалізації різноманітних функцій, таких як керування поведінкою елемента
// або виклик контекстного меню з варіантами дій для користувача.

// Практичні завдання

// 1 Завдання
document.getElementById("btn-click").addEventListener("click", function() {
  
    const newParagraph = document.createElement("p");
  
    newParagraph.textContent = "New Paragraph";

    const contentSection = document.getElementById("content");

    contentSection.appendChild(newParagraph);
});

// 2 Завдання 
const btnInputCreate = document.getElementById("btn-input-create");

btnInputCreate.addEventListener("click", function() {
    
    const newInput = document.createElement("input");

    newInput.setAttribute("type", "text");
    newInput.setAttribute("placeholder", "Введіть текст");
    newInput.setAttribute("name", "inputName");
 
    const section = document.getElementById("content2");

    section.insertBefore(newInput, btnInputCreate.nextSibling);
});